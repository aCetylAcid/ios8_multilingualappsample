//
//  AppDelegate.h
//  20141009_multilingualAppTest
//
//  Created by aCetylAcid on 2014/10/09.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

