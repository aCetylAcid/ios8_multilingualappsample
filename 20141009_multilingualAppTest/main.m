//
//  main.m
//  20141009_multilingualAppTest
//
//  Created by aCetylAcid on 2014/10/09.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
