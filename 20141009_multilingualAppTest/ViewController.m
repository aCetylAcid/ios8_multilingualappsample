//
//  ViewController.m
//  20141009_multilingualAppTest
//
//  Created by aCetylAcid on 2014/10/09.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UIButton *topButton;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _topImageView.image = [UIImage imageNamed:NSLocalizedString(@"IMG00001", nil)];
    [_topButton setTitle:NSLocalizedString(@"topViewController_topButton_title", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)topButtonTapped:(id)sender {
    // アラートの表示
    if ( !UIAlertController.class ) {
        UIAlertView *alert;
        alert = [[UIAlertView alloc]
                 initWithTitle:@""
                 message:NSLocalizedString(@"topViewController_alert_message", nil)
                 delegate:nil
                 cancelButtonTitle:NSLocalizedString(@"Button_OK", nil)
                 otherButtonTitles:nil, nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    } else {
        UIAlertController *alert;
        alert = [UIAlertController
                 alertControllerWithTitle:@""
                 message:NSLocalizedString(@"topViewController_alert_message", nil)
                 preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction
                          actionWithTitle:@"OK"
                          style:UIAlertActionStyleDefault
                          handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


@end
